## Installation:
```
git clone https://gitlab.com/touchcloud/samples/third-party/abaci.git

If you have set SSH key, you can use
git clone git@gitlab.com:touchcloud/samples/third-party/abaci.git
```
## Dependence:
* Opencv
* go-chart
* zap

### you can install opencv by below steps
1. apt-get -y update
2. (If Docker) apt-get -y install sudo 
3. sudo apt-get -y install build-essential
4. cd abaci
5. sudo make install 

### Abaci server setup
1. You have to go to http://localhost/ , and enter to Sytem Config ([Pic 1](#pic1))
2. At Abaci option, type http://localhost:8080/cam to Url and click Add button ([Pic 2](#pic2))
3. Finally enable Abaci and open Abaci's endpoint to check the data ([Pic 3](#pic3))

#### pic1:
![image](https://gitlab.com/touchcloud/samples/third-party/abaci/-/raw/develop/tutorial/abaciSetup1.png)
#### pic2:
![image](https://gitlab.com/touchcloud/samples/third-party/abaci/-/raw/develop/tutorial/abaciSetup2.png)
#### pic3:
![image](https://gitlab.com/touchcloud/samples/third-party/abaci/-/raw/develop/tutorial/abaciSetup3.png)

## Usage:
```
- cd abaci
- ./AbaciDemo
```

## FAQ:
```
Q: ./AbaciDemo: error while loading shared libraries: libopencv_highgui.so.4.2: cannot open shared object file: No such file or directory

A: Your computer may not install Opencv
```