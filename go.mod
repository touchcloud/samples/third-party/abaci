module AbaciDemo

go 1.13

require (
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/ozankasikci/go-image-merge v0.2.2
	github.com/wcharczuk/go-chart v2.0.2-0.20190910040548-3a7bc5543113+incompatible
	go.uber.org/zap v1.13.0
	gocv.io/x/gocv v0.22.0
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1 // indirect
)
