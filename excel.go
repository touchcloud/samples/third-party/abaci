package main

import (
	"encoding/csv"
	"io"
	"os"
)

var flowCounterHeader = []string{"CamName", "From", "To", "Count", "Label", "Time", "Total"}
var crowdDetectHeader = []string{"CamName", "Item", "Count", "Label", "Time"}

func IsExists(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		return false
	}
	return true
}

func writeCsv(dataType, dataTime string, dataHeader []string, data ...string) {
	file := new(os.File)
	w := new(csv.Writer)
	dataSlice := []string{}

	if !IsExists(currentDirectory + "/chart/" + dataType + "_" + dataTime + ".csv") {
		file, err = os.Create(currentDirectory + "/chart/" + dataType + "_" + dataTime + ".csv")
		if err != nil {
			sugarLogger.Error("Failed to create csv:", err)
		}

		//Write UTF-8 Bom
		file.WriteString("\xEF\xBB\xBF")

		w = csv.NewWriter(file)
		w.Write(dataHeader)
	} else {
		file, err = os.OpenFile(currentDirectory+"/chart/"+dataType+"_"+dataTime+".csv", os.O_RDWR|os.O_CREATE, 0666)
		if err != nil {
			sugarLogger.Error("Failed to open csv:", err)
		}
		file.Seek(0, io.SeekEnd)

		w = csv.NewWriter(file)
	}
	defer file.Close()

	for _, d := range data {
		dataSlice = append(dataSlice, d)
	}

	err = w.Write(dataSlice)
	if err != nil {
		sugarLogger.Error("Failed to write:", err)
	}
	w.Flush()
}
