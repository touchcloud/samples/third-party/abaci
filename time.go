package main

import (
	"fmt"
	"time"
)

func GetTimeNow() time.Time {
	t := time.Now()
	local_location, err := time.LoadLocation("UTC")
	if err != nil {
		fmt.Println(err)
	}
	timeNow := t.In(local_location)
	return timeNow
}

func GetTimeDifferent(timeString string, timeNow time.Time) float64 {

	stringTime := timeString
	loc, _ := time.LoadLocation("UTC")
	timeAfter, _ := time.ParseInLocation("2006-01-02 15:04:05", stringTime, loc)

	timeDiff := int(timeAfter.Sub(timeNow).Seconds())
	return float64(timeDiff)
}

func GetDate(timeString string) string {
	loc, _ := time.LoadLocation("UTC")
	sugarLogger.Info(timeString)
	dataTime, _ := time.ParseInLocation("2006-01-02 15:04:05", timeString, loc)

	return dataTime.Format("2006-01-02")
}
