package main

import (
	"fmt"
	"image"
	"image/color"
	"image/png"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"

	"github.com/wcharczuk/go-chart"
	"gocv.io/x/gocv"
)

var imgShow = gocv.Mat{}
var flowchartTimePeriod = 60.0
var crowdChartTimePeriod = 1.0

type cameraChart interface {
	drawChart(camID string, cam camera, currentUsed []string)
}

type flowChart struct {
	counter flowCounter
	xticks  []float64
	yTicks  []float64
	sync.RWMutex
}

type crowdChart struct {
	counter crowdCounter
	xTicks  []float64
	yTicks  []float64
	sync.RWMutex
}

func newFlowChart() flowChart {
	return flowChart{
		counter: make(flowCounter, defaultCamNumber),
	}
}

func newCrowdChart() crowdChart {
	return crowdChart{
		counter: make(crowdCounter, defaultCamNumber),
	}
}

func (flowchart *flowChart) addXTicks(tick float64) {
	flowchart.xticks = append(flowchart.xticks, tick)
}

func (flowchart *flowChart) addYTicks(tick float64) {
	flowchart.yTicks = append(flowchart.yTicks, tick)
}

func (crowdchart *crowdChart) addXTicks(tick float64) {
	crowdchart.xTicks = append(crowdchart.xTicks, tick)
}

func (crowdchart *crowdChart) addYTicks(tick float64) {
	crowdchart.yTicks = append(crowdchart.yTicks, tick)
}

func findMaxfloat(nums []float64) float64 {
	var max = 0.0
	for _, num := range nums {
		if num > max {
			max = num
		}
	}
	return max
}

func checkTickExsit(value float64, ticks []float64) bool {
	for _, v := range ticks {
		if v == value {
			return true
		}
	}
	return false
}

func selectTick(ticks []float64) []float64 {

	maxTick := findMaxfloat(ticks)

	newTick := []float64{}
	if len(ticks) < 10 {
		newTick = ticks
	} else {
		for i := 0; i <= 10; i++ {
			newTick = append(newTick, math.Floor(maxTick*0.1*float64(i)))
		}
	}
	return newTick
}

func clearDirectory(path string) {
	if !strings.HasSuffix(path, "/") {
		path = path + "/"
	}
	_, err := os.Stat(path)
	if err != nil {
		sugarLogger.Error(path, "is not exist.")
	}
	if err == nil {
		os.RemoveAll(path)
	}
}

func checkDirectory(path string) {
	if !strings.HasSuffix(path, "/") {
		path = path + "/"
	}
	finfo, err := os.Stat(path)
	if err != nil {
		os.Mkdir(path, os.ModePerm)
	} else {
		if !finfo.IsDir() {
			os.Mkdir(path, os.ModePerm)
		}
	}
}

func isExist(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil || os.IsExist(err)
}

func NewWhiteImage(width, height int, imagePath string) {
	img := image.NewRGBA(image.Rect(0, 0, width, height))

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			img.Set(x, y, color.White)
		}
	}
	file, err := os.Create(imagePath)
	if err != nil {
		sugarLogger.Error(err)
		fmt.Println(err)
	}
	defer file.Close()
	png.Encode(file, img)

}

func mergeChart(cams cameras) string {
	grids := []*Grid{}

	checkDirectory(currentDirectory + "/chart")

	dir, err := os.Open(currentDirectory + "/chart/")
	if err != nil {
		sugarLogger.Error("Failed to open chart directory.\n")
	}
	defer dir.Close()

	currentCam := getCurrentUsedCam(cams)

	paths := []string{}

	for _, Name := range currentCam {
		if isExist(currentDirectory + "/chart/" + Name + "_FlowCount.png") {
			paths = append(paths, currentDirectory+"/chart/"+Name+"_FlowCount.png")
		} else {
			NewWhiteImage(1024, 400, currentDirectory+"/chart/"+Name+"_FlowCount.png")
			paths = append(paths, currentDirectory+"/chart/"+Name+"_FlowCount.png")
		}
		if isExist(currentDirectory + "/chart/" + Name + "_CrowdDetect.png") {
			paths = append(paths, currentDirectory+"/chart/"+Name+"_CrowdDetect.png")
		} else {
			NewWhiteImage(1024, 400, currentDirectory+"/chart/"+Name+"_CrowdDetect.png")
			paths = append(paths, currentDirectory+"/chart/"+Name+"_FlowCount.png")
		}
	}

	sort.Strings(paths)
	for _, filepath := range paths {
		grids = append(grids, &Grid{ImageFilePath: filepath})
	}
	if len(grids) >= 2 {
		rgba, err := New(grids, 2, int(math.Ceil(float64(len(grids)/2)))).Merge()
		if err != nil {
			sugarLogger.Error("Fialed to merge:", err)
			return ""
		}

		mergeImagePath := currentDirectory + "/chart/merge.png"

		file, err := os.Create(mergeImagePath)
		defer file.Close()
		err = png.Encode(file, rgba)
		if err != nil {
			sugarLogger.Error("Failed to encode: %v", err)
			return ""

		}

		return mergeImagePath
	} else if len(grids) == 1 {
		return paths[0]
	}

	return ""

}

func readChart(imagePath string) {

	img := gocv.IMRead(imagePath, gocv.IMReadColor)
	defer img.Close()
	if img.Empty() {
		return
	}

	img.CopyTo(&imgShow)
}

func showChart(window *gocv.Window, wg *sync.WaitGroup) {
	imgShow = gocv.IMRead(currentDirectory+"/chart/"+"white.png", gocv.IMReadColor)

	if imgShow.Empty() {
		sugarLogger.Error("Failed to read image from:", currentDirectory+"/chart/"+"white.png")
		return
	}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			window.IMShow(imgShow)
			if window.WaitKey(1000) >= 0 {
				break
			}
		}
	}()
}

func (flowchart *flowChart) DrawChart(camID string, cams cameras, currentUsed []string) {

	checkDirectory(currentDirectory + "/chart")

	graph := chart.Chart{
		Title: cams[camID].CamName + "_FlowCount",

		Background: chart.Style{
			Padding: chart.Box{
				Top:  20,
				Left: 260,
			},
		},

		XAxis: chart.XAxis{
			ValueFormatter: func(v interface{}) string {
				if vf, isFloat := v.(float64); isFloat {
					return fmt.Sprintf("%d", int(vf))
				}
				return ""
			},
			Ticks: []chart.Tick{},
			Range: &chart.ContinuousRange{
				Min: 0,
			},
		},

		YAxis: chart.YAxis{
			ValueFormatter: func(v interface{}) string {
				if vf, isFloat := v.(float64); isFloat {
					return fmt.Sprintf("%d", int(vf))
				}
				return ""
			},
			Ticks: []chart.Tick{},
		},

		Series: []chart.Series{},
	}

	tmpSeries := []chart.Series{}
	for fromName, from := range flowchart.counter {
		for toName, to := range from {
			for Label, cntr := range to {
				if checkCounterExist(fromName, currentUsed) && checkCounterExist(toName, currentUsed) {
					tmpSeries = append(tmpSeries, chart.ContinuousSeries{
						Name:    fromName + " To " + toName + " " + Label,
						XValues: cntr.xValues,
						YValues: cntr.yValues,
					})
				}
			}
		}
	}
	graph.Series = tmpSeries

	xFlowTicks := selectTick(cams[camID].flowChart.xticks)
	tmpXTicks := []chart.Tick{}
	for _, v := range xFlowTicks {
		tmpXTicks = append(tmpXTicks, chart.Tick{
			Value: v,
			Label: strconv.Itoa(int(v)),
		})
	}
	graph.XAxis.Ticks = tmpXTicks

	yFlowTicks := selectTick(cams[camID].flowChart.yTicks)
	tmpYTicks := []chart.Tick{}
	for _, v := range yFlowTicks {
		tmpYTicks = append(tmpYTicks, chart.Tick{
			Value: v,
			Label: strconv.Itoa(int(v)),
		})
	}
	graph.YAxis.Ticks = tmpYTicks

	graph.Elements = []chart.Renderable{
		chart.LegendLeft(&graph),
	}

	f, _ := os.Create("./chart/" + cams[camID].CamName + "_" + camID + "_FlowCount.png")
	defer f.Close()

	graph.Render(chart.PNG, f)

}

func (crowdchart *crowdChart) DrawChart(camID string, cams cameras, currentUsed []string) {
	checkDirectory(currentDirectory + "/chart")

	graph := chart.Chart{
		Title: cams[camID].CamName + "_CrowdDetect",

		Background: chart.Style{
			Padding: chart.Box{
				Top:  20,
				Left: 260,
			},
		},
		XAxis: chart.XAxis{
			ValueFormatter: func(v interface{}) string {
				if vf, isFloat := v.(float64); isFloat {
					return fmt.Sprintf("%d", int(vf))
				}
				return ""
			},
			Ticks: []chart.Tick{},
		},
		YAxis: chart.YAxis{
			ValueFormatter: func(v interface{}) string {
				if vf, isFloat := v.(float64); isFloat {
					return fmt.Sprintf("%d", int(vf))
				}
				return ""
			},
			Ticks: []chart.Tick{},
		},
		Series: []chart.Series{},
	}

	tmpSeries := []chart.Series{}
	for itemName, item := range crowdchart.counter {
		for label, cntr := range item {
			if checkCounterExist(itemName, currentUsed) {
				tmpSeries = append(tmpSeries, chart.ContinuousSeries{
					Name:    itemName + " " + label,
					XValues: cntr.xValues,
					YValues: cntr.yValues,
				})
			}
		}
	}
	graph.Series = tmpSeries

	xCrowdTicks := selectTick(cams[camID].crowdChart.xTicks)
	tmpXTicks := []chart.Tick{}
	for _, v := range xCrowdTicks {
		tmpXTicks = append(tmpXTicks, chart.Tick{
			Value: v,
			Label: "",
		})
	}
	graph.XAxis.Ticks = tmpXTicks

	yCrowdTicks := selectTick(cams[camID].crowdChart.yTicks)
	tmpYTicks := []chart.Tick{}
	for _, v := range yCrowdTicks {
		tmpYTicks = append(tmpYTicks, chart.Tick{
			Value: v,
			Label: strconv.Itoa(int(v)),
		})
	}
	graph.YAxis.Ticks = tmpYTicks

	graph.Elements = []chart.Renderable{
		chart.LegendLeft(&graph),
	}

	f, _ := os.Create("./chart/" + cams[camID].CamName + "_" + camID + "_CrowdDetect.png")
	defer f.Close()

	graph.Render(chart.PNG, f)
}
