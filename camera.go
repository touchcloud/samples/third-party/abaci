package main

import (
	"fmt"
	"log"
	"sort"
	"strconv"
	"sync"
	"time"
)

type camFlowCount struct {
	Version   string `json:"version"`
	Location  string `json:"localtion"`
	CamID     string `json:"camid"`
	CamName   string `json:"camname"`
	Time      string `json:"time"`
	From      string `json:"from"`
	FromID    string `json:"fromid"`
	To        string `json:"to"`
	ToID      string `json:"toid"`
	Direction string `json:"direction"`
	Label     string `json:"label"`
	Count     int    `json:"count"`
	TimeZone  string `json:"timezone"`
}

type camCrowdDetect struct {
	Version   string `json:"version"`
	Location  string `json:"localtion"`
	CamID     string `json:"camid"`
	CamName   string `json:"camname"`
	Time      string `json:"time"`
	Item      string `json:"item"`
	ItemID    string `json:"itemid"`
	Direction string `json:"direction"`
	Label     string `json:"label"`
	Count     int    `json:"count"`
	TimeZone  string `json:"timezone"`
}

type camera struct {
	CamName   string
	flowCount []camFlowCount
	flowChart flowChart

	crowdDetect []camCrowdDetect
	crowdChart  crowdChart
}
type cameras map[string]*camera

var defaultCamNumber = 100
var defaultCounterNumber = 30
var defaultLabelNumber = 10

func newCamera(camName string) *camera {
	cam := &camera{
		CamName:     camName,
		flowCount:   []camFlowCount{},
		flowChart:   newFlowChart(),
		crowdDetect: []camCrowdDetect{},
		crowdChart:  newCrowdChart(),
	}

	return cam
}

//map[camID]*Camera
var cams = make(cameras, defaultCamNumber)

type usedCamTimer struct {
	usedMap map[string]time.Time
	sync.RWMutex
}

func newUsedCamTimer(camNumber int) *usedCamTimer {
	uct := new(usedCamTimer)
	uct.usedMap = make(map[string]time.Time, camNumber)
	return uct
}

//var usedCamTimer = make(map[string]time.Time, defaultCamNumber)
var usedCamTimers = newUsedCamTimer(defaultCamNumber)

func setUsedCamTimer(field string, timers *usedCamTimer) {
	timers.Lock()
	timers.usedMap[field] = GetTimeNow()
	timers.Unlock()
}

func getUselessCam(uct *usedCamTimer, period float64) []string {
	var uselessCam []string
	uct.Lock()
	for cam, camTime := range uct.usedMap {
		if cam != "base" {
			timeDiff := uct.usedMap["base"].Sub(camTime).Seconds()
			if timeDiff > period {
				uselessCam = append(uselessCam, cam)
			}
		}
	}
	uct.Unlock()

	return uselessCam
}

func deleteUselessCam(cams cameras, uct *usedCamTimer, uselessCam []string) {
	for _, camID := range uselessCam {
		delete(cams, camID)
		delete(uct.usedMap, camID)
	}
}

func getCurrentUsedCam(cams cameras) []string {
	var currentCam []string
	for camID, cam := range cams {
		currentCam = append(currentCam, cam.CamName+"_"+camID)
	}
	return currentCam
}

func clearCmd() {
	fmt.Println("\033c")
}

func PrintCam(cameras cameras) {

	sortedKey := make([]string, 0)
	for k, _ := range cameras {
		sortedKey = append(sortedKey, k)
	}
	sort.Strings(sortedKey)

	clearCmd()
	log.Println("Channel number: " + strconv.Itoa(len(cameras)))

	for _, camID := range sortedKey {
		camInfo := cameras[camID]
		fmt.Printf("Camera Name: %s\n", camInfo.CamName)

		fmt.Printf("Flow Counting:\n")
		for _, cam := range camInfo.flowCount {
			fmt.Printf("\tCamName: %15s | From: %10s | To: %10s | Count:%4d | Label: %10s | Time: %20s | Total: %4d |\n", cam.CamName, cam.From, cam.To, cam.Count, cam.Label, cam.Time, camInfo.flowChart.counter[cam.From][cam.To][cam.Label].totalcount)
		}

		fmt.Printf("-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n")

		fmt.Printf("Crowd Detection:\n")
		for _, cam := range camInfo.crowdDetect {
			fmt.Printf("\tCamName: %15s | Item: %10s | Count: %4d | Label: %10s | Time: %20s |\n", cam.CamName, cam.Item, cam.Count, cam.Label, cam.Time)
		}
		fmt.Printf("\n\n\n")
	}
}
