package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	_ "net/http/pprof"
	"os"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"gocv.io/x/gocv"
)

var err error
var currentDirectory, _ = os.Getwd()

var initTime time.Time

func init() {
	InitLogger()
	defer sugarLogger.Sync()

	initTime = GetTimeNow()

	clearDirectory(currentDirectory + "/chart")
	checkDirectory(currentDirectory + "/chart")
	NewWhiteImage(1024, 400, currentDirectory+"/chart/"+"white.png")
}

func cameraInformer(context *gin.Context) {
	context.String(http.StatusOK, "Touchcloud abaci CameraInformer")

	b, err := ioutil.ReadAll(context.Request.Body)
	if err != nil {
		sugarLogger.Error("Read failed:", err)
	}

	setUsedCamTimer("base", usedCamTimers)

	var in interface{}
	var currentUsed []string

	err = json.Unmarshal(b, &in)
	sugarLogger.Info(in, "\n\n")
	switch in := in.(type) {
	//FlowCount format
	case []interface{}:
		//no catch Data
		if len(in) == 0 {
			return
		}
		var camFlow camFlowCount
		var camFlowCounts []camFlowCount

		for _, feature := range in {
			jsonStr, err := json.Marshal(feature)
			err = json.Unmarshal([]byte(jsonStr), &camFlow)
			if err != nil {
				sugarLogger.Error("json format error:", err)
			}

			if _, ok := cams[camFlow.CamID]; !ok {
				cams[camFlow.CamID] = newCamera(camFlow.CamName)
			}

			cams[camFlow.CamID].flowChart.newFlowCounter(camFlow.CamID, camFlow.From, camFlow.To, camFlow.Label, cams)

			camFlowCounts = append(camFlowCounts, camFlow)

			timeDiff := GetTimeDifferent(camFlow.Time, initTime)
			if timeDiff > 0 {
				cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].addXValues(math.Floor(timeDiff / flowchartTimePeriod))
				if !checkTickExsit(math.Floor(timeDiff/flowchartTimePeriod), cams[camFlow.CamID].flowChart.xticks) {
					cams[camFlow.CamID].flowChart.addXTicks(math.Floor(timeDiff / flowchartTimePeriod))
				}
				cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].addYValues(float64(cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].totalcount))

				if !checkTickExsit(float64(cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].totalcount), cams[camFlow.CamID].flowChart.yTicks) {
					cams[camFlow.CamID].flowChart.addYTicks(float64(cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].totalcount))
				}

				cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].setTotalCount(cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].totalcount + camFlow.Count)
				cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].setData(camFlow.From, camFlow.To)
				cams[camFlow.CamID].flowChart.counter[camFlow.From][camFlow.To][camFlow.Label].setLabel(camFlow.Label)

				currentUsed = append(currentUsed, camFlow.From)
			}

			//writeCsv("FlowCounter", GetDate(camFlow.Time), flowCounterHeader, camFlow.CamName, camFlow.From, camFlow.To, strconv.Itoa(camFlow.Count), camFlow.Label, camFlow.Time)
		}
		cams[camFlow.CamID].flowCount = camFlowCounts
		setUsedCamTimer(camFlow.CamID, usedCamTimers)
		cams[camFlow.CamID].flowChart.DrawChart(camFlow.CamID, cams, currentUsed)

	//CrowdDetect format
	case string:
		mmap := make(map[string]interface{})
		err = json.Unmarshal([]byte(in), &mmap)
		if err != nil {
			sugarLogger.Error("json format error:", err)
		}

		mdata := mmap["Data"].([]interface{})

		if len(mdata) > 0 {
			var camCrowd camCrowdDetect
			var camCrowdDetects []camCrowdDetect

			for _, feature := range mdata {
				jsonStr, err := json.Marshal(feature)
				err = json.Unmarshal([]byte(jsonStr), &camCrowd)
				if err != nil {
					sugarLogger.Error("json format error:", err)
				}

				if _, ok := cams[camCrowd.CamID]; !ok {
					cams[camCrowd.CamID] = newCamera(camCrowd.CamName)
				}
				cams[camCrowd.CamID].crowdChart.newCrowdCounter(camCrowd.CamID, camCrowd.Item, camCrowd.Label, cams)

				camCrowdDetects = append(camCrowdDetects, camCrowd)

				timeDiff := GetTimeDifferent(camCrowd.Time, initTime)
				if timeDiff > 0 {
					cams[camCrowd.CamID].crowdChart.counter[camCrowd.Item][camCrowd.Label].addXValues(math.Floor(timeDiff / crowdChartTimePeriod))
					if !checkTickExsit(math.Floor(timeDiff/crowdChartTimePeriod), cams[camCrowd.CamID].crowdChart.xTicks) {
						cams[camCrowd.CamID].crowdChart.addXTicks(math.Floor(timeDiff / crowdChartTimePeriod))
					}

					cams[camCrowd.CamID].crowdChart.counter[camCrowd.Item][camCrowd.Label].addYValues(float64(camCrowd.Count))
					if !checkTickExsit(float64(camCrowd.Count), cams[camCrowd.CamID].crowdChart.yTicks) {
						cams[camCrowd.CamID].crowdChart.addYTicks(float64(camCrowd.Count))
					}

					cams[camCrowd.CamID].crowdChart.counter[camCrowd.Item][camCrowd.Label].setTotalCount(cams[camCrowd.CamID].crowdChart.counter[camCrowd.Item][camCrowd.Label].totalcount + camCrowd.Count)
					cams[camCrowd.CamID].crowdChart.counter[camCrowd.Item][camCrowd.Label].setData(camCrowd.Item)
					cams[camCrowd.CamID].crowdChart.counter[camCrowd.Item][camCrowd.Label].setLabel(camCrowd.Label)
					currentUsed = append(currentUsed, camCrowd.Item)
				}

				//writeCsv("CrowdDetect", GetDate(camCrowd.Time), crowdDetectHeader, camCrowd.CamName, camCrowd.Item, strconv.Itoa(camCrowd.Count), camCrowd.Label, camCrowd.Time)
			}
			cams[camCrowd.CamID].crowdDetect = camCrowdDetects
			setUsedCamTimer(camCrowd.CamID, usedCamTimers)
			cams[camCrowd.CamID].crowdChart.DrawChart(camCrowd.CamID, cams, currentUsed)
		}
	}
	deleteUselessCam(cams, usedCamTimers, getUselessCam(usedCamTimers, 180.0))
	PrintCam(cams)

	mergeImagePath := mergeChart(cams)
	if mergeImagePath != "" {
		readChart(mergeImagePath)
	}

}

func main() {
	window := gocv.NewWindow("chart")
	window.ResizeWindow(2048, 800)
	defer window.Close()
	wg := sync.WaitGroup{}

	fmt.Println("Connecting Service...")
	fmt.Println("Successful connected")

	defer imgShow.Close()
	showChart(window, &wg)

	/*
		go func() {
			http.ListenAndServe("0.0.0.0:8000", nil)
		}()
	*/

	router := gin.Default()
	router.POST("/cam", cameraInformer)
	router.Run(":8080")

	wg.Wait()

}
