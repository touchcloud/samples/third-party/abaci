package main

import ()

type counter struct {
	totalcount int
	xValues    []float64
	yValues    []float64
	label      string

	data interface{}
}
type flowData struct {
	from string
	to   string
}

type crowdData struct {
	item string
}

//map[from][to][label]counter
type flowCounter map[string]map[string]map[string]*counter

//map[item][label]counter
type crowdCounter map[string]map[string]*counter

func (flowchart *flowChart) newFlowCounter(CamID, From, To, Label string, cams cameras) {
	flowchart.Lock()
	if _, ok := flowchart.counter[From]; !ok {
		flowchart.counter[From] = make(map[string]map[string]*counter, defaultCounterNumber)
		flowchart.counter[From][To] = make(map[string]*counter, defaultLabelNumber)
		flowchart.counter[From][To][Label] = &counter{}
	}
	if _, ok := flowchart.counter[From][To]; !ok {
		flowchart.counter[From][To] = make(map[string]*counter, defaultLabelNumber)
		flowchart.counter[From][To][Label] = &counter{}
	}
	if _, ok := flowchart.counter[From][To][Label]; !ok {
		flowchart.counter[From][To][Label] = &counter{}
	}
	flowchart.Unlock()
}

func (crowdchart *crowdChart) newCrowdCounter(CamID, Item, Label string, cams cameras) {
	crowdchart.Lock()
	if _, ok := crowdchart.counter[Item]; !ok {
		crowdchart.counter[Item] = make(map[string]*counter, defaultLabelNumber)
		crowdchart.counter[Item][Label] = &counter{}
	}
	if _, ok := crowdchart.counter[Item][Label]; !ok {
		crowdchart.counter[Item][Label] = &counter{}
	}
	crowdchart.Unlock()
}

func (cntr *counter) addXValues(value float64) {
	cntr.xValues = append(cntr.xValues, value)
}

func (cntr *counter) addYValues(value float64) {
	cntr.yValues = append(cntr.yValues, value)
}

func (cntr *counter) setTotalCount(value int) {
	cntr.totalcount = value
}

func (cntr *counter) setLabel(value string) {
	cntr.label = value
}

func (cntr *counter) setData(value ...string) {
	switch d := cntr.data.(type) {
	case flowData:
		d.from = value[0]
		d.to = value[1]

	case crowdData:
		d.item = value[0]
	}

}

func checkCounterExist(value string, counter []string) bool {
	for _, v := range counter {
		if v == value {
			return true
		}
	}
	return false
}
